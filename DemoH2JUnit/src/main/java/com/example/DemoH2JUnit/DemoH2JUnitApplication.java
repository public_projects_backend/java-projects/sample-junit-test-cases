package com.example.DemoH2JUnit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@ComponentScan(basePackages = "com.example.DemoH2JUnit")
public class DemoH2JUnitApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoH2JUnitApplication.class, args);
		System.out.println("Test the application");
	}

}
