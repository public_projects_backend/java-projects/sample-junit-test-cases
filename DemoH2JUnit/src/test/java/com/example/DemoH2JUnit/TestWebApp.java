package com.example.DemoH2JUnit;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.example.DemoH2JUnit.model.Employee;
import com.example.DemoH2JUnit.service.interfaces.EmployeeService;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class TestWebApp extends DemoH2JUnitApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private EmployeeService employeeService;

	@Test
	@Order(1)
	public void fetchEmployeesListApi() throws Exception {
		mockMvc.perform(get("/api/employees")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;")).andExpect(jsonPath("$[0].id").value("1"))
				.andExpect(jsonPath("$[1].name").value("Kumar"));
	}

	@Test
	@Order(2)
	public void fetchEmployeeByIdApi() {
		Long id = (long) 1;
		Employee employee = employeeService.getEmployee(id);
		assertEquals("Chethan", employee.getName());
	}
	
	@Test
	@Order(3)
	public void updateEmployeeApi() {
		Long id = (long) 1;
		Employee employee = employeeService.getEmployee(id);
		employee.setName("Test Name");
		employeeService.updateEmployee(employee);
		employee = employeeService.getEmployee(id);
		assertEquals("Test Name", employee.getName());
	}
	
	@Test
	@Order(4)
	public void saveEmployeeApi() {
		Long id = (long) 4;
		Employee employee = new Employee();
		employee.setName("Test User");
		employee.setDepartment("Dev");
		employee.setSalary(32145);
		employeeService.saveEmployee(employee);
		employee = employeeService.getEmployee(id);
		assertEquals("Test User", employee.getName());
	}
	
	@Test
	@Order(5)
	public void deleteEmployeeApi() {
		Long id = (long) 4;
		employeeService.deleteEmployee(id);
		assertEquals(3, employeeService.retrieveEmployees().size());
	}
}